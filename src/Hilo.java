public class Hilo extends Thread{
    private int numero1;
    private int numero2;

    public Hilo(int numero1, int numero2){
        this.numero1 = numero1;
        this.numero2 = numero2;
    }
    @Override
    public void run(){
        for(int i=numero1; i<=numero2; i++){
            System.out.println("Número: "+i);
        }
    }
}
