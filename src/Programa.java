public class Programa {
    public static void main(String[] args){
        Hilo hilo1 = new Hilo(1, 9);
        Hilo hilo2 = new Hilo(22, 33);
                        //Siempre que se utilicen ".join()", hay que utilizar un try{....}catch{....}.
        hilo1.start();
        try{
            hilo1.join();
        }catch(InterruptedException ie) {
            ie.printStackTrace();
        }
        hilo2.start();
    }

}
